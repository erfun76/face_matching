## README
----------------
## Dependencies
Install dependencies from requirement.txt

- requirements.txt

Run following command
```bash
pip install -r requirements.txt  # install dependencies
```
----------------
## Test
You can verify one person using its input image and database like this:

```python
matcher = FaceMatching(root = './test', name = 'buffalo_l', thresh = 0.4)
matcher.verify(query_image, list_of_person_database_images)
```  
- name is the name of package you want to use. it can be choosen from model_zoo or your self build model.
- root is the pass for detection and recognition models that by default they will be downloaded in directory like 
![model_path_img](figures/model_sample.png)


You can also download .onnx file from [model_zoo](https://github.com/deepinsight/insightface/tree/master/model_zoo) and replace it in given directory with other models.

- thresh is used for detecting similar pair as if their similarity measure are more than thresh, they are considered as similar pairs.


For testing class run test.py

----------------
## Dataset
You can also download lfw dataset from [here](http://vis-www.cs.umass.edu/lfw/lfw.tgz).
