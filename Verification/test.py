import cv2
from matcher import FaceMatching

# input image path
im1 = cv2.imread('./images/erfun2.jpg')
# database images path
person_id = "erfan_zeinivand"

# database = []
# for im2_path in img_paths:
#     database.append(cv2.imread(im2_path))


matcher = FaceMatching(database="./database")
print(matcher.verify(im1, person_id))
