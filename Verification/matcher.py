import numpy as np
from insightface.app import FaceAnalysis
import os
import cv2

# python-package base
class FaceMatching():
    def __init__(self, root = './models', name = 'buffalo_l',  thresh = 0.4, database="./database", feature_take_algorithm="nearest_neighbour"):
        self.app = FaceAnalysis(root= root, name = name, allowed_modules=['detection', 'recognition'])
        self.app.prepare(ctx_id=0, det_size=(640,640))
        self.thresh = thresh
        self.feature_take_algorithm = feature_take_algorithm
        self.database = database

    def verify(self, query, pid):
        im1 = query
        faces = self.app.get(im1)
        feat1 = faces[0].normed_embedding
        feat1 = np.array(feat1, dtype=np.float32)
        
        pid_path = os.path.join(self.database, pid)
        database_feats = []
        for im_path in os.listdir(pid_path):
            im_path = os.path.join(pid_path, im_path)
            im2 = cv2.imread(im_path)
            faces = self.app.get(im2)
            feat = faces[0].normed_embedding
            feat = np.array(feat, dtype=np.float32)
            database_feats.append(feat)
        
        feat2 = self.__select_feat(database_feats, self.feature_take_algorithm)
        
        # TODO: add other similarity distance
        sims = self.__calc_similarity(feat1, feat2)

        ab_sims = abs(sims)
        return ab_sims, (ab_sims>self.thresh)

    # TODO: add other algorithms
    def __select_feat(self, feats, algorithm="mean"):
        if algorithm=="mean":
            feat = np.mean(feats, axis=0)
        elif algorithm=="nearest_neighbour":
            feat = nearest_neighbour(feats)
        elif algorithm=="nearest_to_mean":
            feat = nearest_to_mean(feats)
        return feat

    def __calc_similarity(self, feat1, feat2, algorithm="cosine"):
        if algorithm == "cosine":
            sims = np.dot(feat1, feat2)
        return sims

    def verify_im(self, query, key):
        im1 = query
        im2 = key
        faces = self.app.get(im1)
        feat1 = faces[0].normed_embedding
        feat1 = np.array(feat1, dtype=np.float32)
        
        faces = self.app.get(im2)
        feat2 = faces[0].normed_embedding
        feat2 = np.array(feat2, dtype=np.float32)
        
        # TODO: add other similarity distance
        sims = self.__calc_similarity(feat1, feat2)

        ab_sims = abs(sims)
        return ab_sims, (ab_sims>self.thresh)

def nearest_to_mean(feats):
    feats = np.array(feats)
    feature_mean = np.mean(feats, axis=0)
    similarity = np.matmul(feature_mean, feats.T)
    index = np.argmax(similarity)
    return feats[index]

def nearest_neighbour(feats):
    feats = np.array(feats)
    similarity = np.matmul(feats, feats.T)
    # most similar to other neighbors
    index = np.argmax(np.sum(similarity, axis=1))
    return feats[index]
            

