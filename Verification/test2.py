import cv2
from matcher import FaceMatching

# input image path
im1 = cv2.imread('./images/erfun2.jpg')
im2 = cv2.imread('./images/erfun1.jpeg')


matcher = FaceMatching()
print(matcher.verify_im(im1, im2))
