import os
import datetime
import sklearn.preprocessing
import numpy as np
from numpy.linalg import norm as l2norm
from skimage import transform as trans

import insightface
import mxnet as mx
from mxnet import ndarray as nd
# from detection_model.yolo5.detector import Detection
from insightface.app import FaceAnalysis

arcface_src = np.array(
    [[38.2946, 51.6963], [73.5318, 51.5014], [56.0252, 71.7366],
     [41.5493, 92.3655], [70.7299, 92.2041]],
    dtype=np.float32)
arcface_src = np.expand_dims(arcface_src, axis=0)

class Embedding():
    '''
    a class for extracting features from a batch of images.

    '''
    def __init__(self, model_path, batch_size, image_size = [112, 112]):
        self.nets = []
        self.image_size = image_size
        epochs = []
        # self.detector = Detection()
        self.detector = FaceAnalysis(name = 'buffalo_sc',root= '/content/test',allowed_modules=['detection'])
        self.detector.prepare(ctx_id=0, det_size=(640,640))
        # mxnet
        self.ctx = mx.gpu(0)
        # model epoch
        prefix = model_path.split(',')[0]
        pdir = os.path.dirname(prefix)
        for fname in os.listdir(pdir):
            if not fname.endswith('.params'):
                continue
            _file = os.path.join(pdir, fname)
            if _file.startswith(prefix):
                epoch = int(fname.split('.')[0].split('-')[1])
                epochs.append(epoch)
        epochs = sorted(epochs, reverse=True)
        print('model number', len(epochs))

        # load models
        time0 = datetime.datetime.now()
        for epoch in epochs:
            print('loading', prefix, epoch)
            sym, arg_params, aux_params = mx.model.load_checkpoint(prefix, epoch)
            all_layers = sym.get_internals()
            sym = all_layers['fc1_output']
            model = mx.mod.Module(symbol=sym, context=self.ctx, label_names=None)
            # model.bind(data_shapes=[('data', (batch_size, 3, image_size[0], image_size[1]))], label_shapes=[('softmax_label', (batch_size,))])
            model.bind(data_shapes=[('data', (batch_size, 3, image_size[0],
                                              image_size[1]))])
            model.set_params(arg_params, aux_params)
            self.nets.append(model)
        time_now = datetime.datetime.now()
        diff = time_now - time0
        print('model loading time', diff.total_seconds())

    def get_embeding(self, images): # (batch_size, 3, image_size[0], image_size[1])
        embeddings = []
        imgs = []
        # detection
        for i, img in enumerate(images):
            # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            # bbox = self.detector.get_bbox(img)
            res = self.detector.get(img)

            if res:
                face_crop = self.__norm_crop(img, np.array(res[0]['kps']))
                # x1, y1, x2, y2 = res[0]['bbox']
                # bbox = [max(0,x1), max(0,y1), x2-x1, y2-y1]
                # bbox = [int(x) for x in bbox]
                # if bbox:
                #     center = [bbox[0]+bbox[2]//2, bbox[1]+bbox[3]//2]
                #     max_crop = max(bbox[2], self.image_size[0], bbox[3], self.image_size[1])
                #     face_crop = img[max(center[1]-max_crop//2,0):center[1]+max_crop//2, max(0, center[0]-max_crop//2):center[0]+max_crop//2]
                #     try:
                #         crop_shape = np.shape(face_crop)
                #         face_crop = cv2.copyMakeBorder(face_crop, 0, max(0, crop_shape[1]-crop_shape[0]), 0, max(0, crop_shape[0]-crop_shape[1]), cv2.BORDER_CONSTANT, 0)
                #         # print(np.shape(face_crop))
                #         imgs.append(cv2.resize(face_crop, (self.image_size[0], self.image_size[1]), interpolation= cv2.INTER_LINEAR))
                        
                #     except: 
                #         print("no detection")
                imgs.append(face_crop)
            else:
                print("no detection")
                imgs.append(cv2.resize(img, (self.image_size[0], self.image_size[1]), interpolation= cv2.INTER_LINEAR))
        
        imgs = np.array(imgs)
        # model input shape (batch_size, channel, w, h)
        if imgs.shape[1] != 3:
            imgs = imgs.transpose((0, 3, 1, 2))

        # dummy input
        _label = nd.ones((len(imgs), ))

        _data = mx.nd.array(imgs)
        db = mx.io.DataBatch(data=(_data, ), label=(_label, ))
        for model in self.nets:
            model.forward(db, is_train=False)
            net_out = model.get_outputs()
            _embeddings = net_out[0].asnumpy()
            embeddings.append(_embeddings)
        del net_out, _embeddings, imgs, db, _data
        self.ctx.empty_cache()
        return embeddings
    
    def __norm_crop(self, img, landmark, image_size=112, mode='arcface'):
        M, pose_index = self.__estimate_norm(landmark, image_size, mode)
        warped = cv2.warpAffine(img, M, (image_size, image_size), borderValue=0.0)
        return warped

    # lmk is prediction; src is template
    def __estimate_norm(self, lmk, image_size=112, mode='arcface'):
        assert lmk.shape == (5, 2)

        tform = trans.SimilarityTransform()
        lmk_tran = np.insert(lmk, 2, values=np.ones(5), axis=1)
        min_M = []
        min_index = []
        min_error = float('inf')
        if mode == 'arcface':
            if image_size == 112:
                src = arcface_src
            else:
                src = float(image_size) / 112 * arcface_src
        else:
            src = src_map[image_size]

        for i in np.arange(src.shape[0]):
            tform.estimate(lmk, src[i])
            M = tform.params[0:2, :]
            results = np.dot(M, lmk_tran.T)
            results = results.T
            error = np.sum(np.sqrt(np.sum((results - src[i])**2, axis=1)))
            #         print(error)
            if error < min_error:
                min_error = error
                min_M = M
                min_index = i
        return min_M, min_index

    def normalize_embeding1(self, embeds):
        normalized_embeddings = []
        for embed in embeds:
            normalized_embeddings.append(sklearn.preprocessing.normalize(embed))
        return normalized_embeddings

    def normalize_embeding2(self, embeds):
        normalized_embeddings = []
        normalized_embeddings_patch = []
        for embed_patch in embeds:
            for embed in embed_patch:
                normalized_embeddings_patch.append(embed/l2norm(embed))
            normalized_embeddings.append(normalized_embeddings_patch)
        return normalized_embeddings